#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

void testExercise_A() {
	// Given
    const char* value[] = {"Eva"};

    // When
    const char* amount = oddEvenExchangedValues(value, 1);

    // Then
    assertArrayEquals_String(value, 1, amount, 1);
}


void testExercise_B() {
	// Given
    const char* value[]= {"Eva", "Javier"};
    const char* finalvalue[] = {"Javier", "Eva"};

    // When
    const char* amount = oddEvenExchangedValues(value, 2);

    // Then
    assertArrayEquals_String(finalvalue, 2, amount, 2);
}

void testExercise_C() {
	// Given
    const char* value[]= {"Eva", "Javier", "Alba"};
    const char* finalvalue[] = {"Javier", "Eva", "Alba"};

    // When
    const char* amount = oddEvenExchangedValues(value, 3);

    // Then
    assertArrayEquals_String(finalvalue, 3, amount, 3);
	}

void testExercise_D() {
	// Given
    const char* value[]= {"Eva", "Javier", "Alba", "Manuel"};
    const char* finalvalue[] = {"Javier", "Eva", "Manuel", "Alba"};

    // When
    const char* amount = oddEvenExchangedValues(value, 4);

    // Then
    assertArrayEquals_String(finalvalue, 4, amount, 4);
	}

void testExercise_E() {
	// Given
    const char* value[]= {"Eva", "Javier", "Alba", "Manuel", "Susana"};
    const char* finalvalue[] = {"Javier", "Eva", "Manuel", "Alba", "Susana"};

    // When
    const char* amount = oddEvenExchangedValues(value, 5);

    // Then
    assertArrayEquals_String(finalvalue, 5, amount, 5);
	}
