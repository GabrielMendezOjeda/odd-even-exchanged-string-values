#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

const char** oddEvenExchangedValues(const char** value, int valueLength) {
    if(valueLength == 1){
    	return value;
    }
    for(int k = 0; k< valueLength -1; k=k+2){
    	const char* devuelve = value[k];
    	value[k] = value[k+1];
    	value[k+1] = devuelve;
    }

	return value;
}
