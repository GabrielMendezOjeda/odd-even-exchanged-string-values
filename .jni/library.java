/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

public class library {
    @executerpane.MethodAnnotation(signature = "oddEvenExchangedValues(const char**,int):const char**")
    public String[] oddEvenExchangedValues(String[] value, int valueLength){
        return oddEvenExchangedValues_(value, valueLength);
    }
    private native String[] oddEvenExchangedValues_(String[] value, int valueLength);


    static {
        System.load(new java.io.File(".jni", "library_jni.so").getAbsolutePath());
    }
}
